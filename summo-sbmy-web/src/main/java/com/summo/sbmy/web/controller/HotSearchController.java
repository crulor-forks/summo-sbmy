package com.summo.sbmy.web.controller;

import com.summo.sbmy.common.model.dto.HotSearchDTO;
import com.summo.sbmy.common.model.dto.HotSearchDetailDTO;
import com.summo.sbmy.common.page.Page;
import com.summo.sbmy.common.result.ResultModel;
import com.summo.sbmy.service.SbmyHotSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.summo.sbmy.common.cache.SbmyHotSearchCache.CACHE_MAP;

@RestController
@RequestMapping("/api/hotSearch")
public class HotSearchController {

    @Autowired
    private SbmyHotSearchService sbmyHotSearchService;

    @GetMapping("/queryByType")
    public ResultModel<HotSearchDetailDTO> queryByType(@RequestParam(required = true) String type) {
        return ResultModel.success(CACHE_MAP.get(type.toUpperCase()));
    }

    @GetMapping("/pageQueryHotSearchByTitle")
    public ResultModel<Page<HotSearchDTO>> pageQueryHotSearchByTitle(@RequestParam(required = true) String title,
        @RequestParam(required = true) Integer pageNum, @RequestParam(required = true) Integer pageSize) {
        return ResultModel.success(sbmyHotSearchService.pageQueryHotSearchByTitle(title, pageNum, pageSize));
    }
}
