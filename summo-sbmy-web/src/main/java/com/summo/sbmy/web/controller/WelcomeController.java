package com.summo.sbmy.web.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.summo.sbmy.common.model.dto.VisitorCountDTO;
import com.summo.sbmy.common.result.ResultModel;
import com.summo.sbmy.common.util.DateUtil;
import com.summo.sbmy.service.SbmyVisitLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/welcome")
public class WelcomeController {
    @Autowired
    private SbmyVisitLogService sbmyVisitLogService;

    @GetMapping("/queryVisitorCount")
    public ResultModel<VisitorCountDTO> queryVisitorCount() {
        Date startTime = DateUtil.getStartOfDay(Calendar.getInstance().getTime());
        Date endTime = DateUtil.getEndOfDay(Calendar.getInstance().getTime());
        return ResultModel.success(sbmyVisitLogService.queryVisitorCount(startTime, endTime));
    }
}
