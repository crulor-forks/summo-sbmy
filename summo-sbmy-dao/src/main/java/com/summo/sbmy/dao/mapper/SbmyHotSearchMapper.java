package com.summo.sbmy.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.summo.sbmy.dao.entity.SbmyHotSearchDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SbmyHotSearchMapper extends BaseMapper<SbmyHotSearchDO> {
}