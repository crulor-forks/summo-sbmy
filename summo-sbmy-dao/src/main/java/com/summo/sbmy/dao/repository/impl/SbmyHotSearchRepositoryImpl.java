package com.summo.sbmy.dao.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.summo.sbmy.dao.entity.SbmyHotSearchDO;
import com.summo.sbmy.dao.mapper.SbmyHotSearchMapper;
import com.summo.sbmy.dao.repository.SbmyHotSearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public class SbmyHotSearchRepositoryImpl extends ServiceImpl<SbmyHotSearchMapper, SbmyHotSearchDO>
    implements SbmyHotSearchRepository {

}
