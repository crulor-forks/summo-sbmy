package com.summo.sbmy.dao.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.summo.sbmy.dao.entity.SbmyVisitLogDO;
import com.summo.sbmy.dao.mapper.SbmyVisitLogMapper;
import com.summo.sbmy.dao.repository.SbmyVisitLogRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class SbmyVisitLogRepositoryImpl extends ServiceImpl<SbmyVisitLogMapper,SbmyVisitLogDO> implements SbmyVisitLogRepository {
    @Override
    public int queryUvByStartTimeAndEndTime(Date startTime, Date endTime) {
        return this.baseMapper.queryUvByStartTimeAndEndTime(startTime, endTime);
    }

    @Override
    public int queryUv() {
        return this.baseMapper.queryUv();
    }
}
