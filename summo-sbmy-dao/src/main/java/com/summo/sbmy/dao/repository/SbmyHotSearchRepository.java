package com.summo.sbmy.dao.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import com.summo.sbmy.dao.entity.SbmyHotSearchDO;

public interface SbmyHotSearchRepository extends IService<SbmyHotSearchDO> {
}
