package com.summo.sbmy.dao.entity;

import java.util.Date;
import javax.persistence.*;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.summo.sbmy.dao.AbstractBaseDO;
import lombok.*;


@Getter
@Setter
@TableName("t_sbmy_visit_log")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SbmyVisitLogDO   extends AbstractBaseDO<SbmyVisitLogDO> {
    /**
     * 物理主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 设备类型，手机还是电脑
     */
    @Column(name = "device_type")
    private String deviceType;

    /**
     * 访问
     */
    private String ip;

    /**
     * IP地址
     */
    private String address;

    /**
     * 耗时
     */
    private Integer time;

    /**
     * 调用方法
     */
    private String method;

    /**
     * 参数
     */
    private String params;
}