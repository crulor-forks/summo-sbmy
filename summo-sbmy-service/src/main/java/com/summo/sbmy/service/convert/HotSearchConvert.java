package com.summo.sbmy.service.convert;

import com.summo.sbmy.common.model.dto.HotSearchDTO;
import com.summo.sbmy.dao.entity.SbmyHotSearchDO;

public class HotSearchConvert {
    public static HotSearchDTO toDTOWhenQuery(SbmyHotSearchDO sbmyHotSearchDO) {
        return HotSearchDTO.builder()
            //热搜作者
            .hotSearchAuthor(sbmyHotSearchDO.getHotSearchAuthor())
            //热搜作者头像
            .hotSearchAuthorAvatar(sbmyHotSearchDO.getHotSearchAuthorAvatar())
            //热搜封面
            .hotSearchCover(sbmyHotSearchDO.getHotSearchCover())
            //热度
            .hotSearchHeat(sbmyHotSearchDO.getHotSearchHeat())
            //热搜id
            .hotSearchId(sbmyHotSearchDO.getHotSearchId())
            //热搜摘要
            .hotSearchExcerpt(sbmyHotSearchDO.getHotSearchExcerpt())
            //热搜排序
            .hotSearchOrder(sbmyHotSearchDO.getHotSearchOrder())
            //热搜来源
            .hotSearchResource(sbmyHotSearchDO.getHotSearchResource())
            //热搜标题
            .hotSearchTitle(sbmyHotSearchDO.getHotSearchTitle())
            //热搜链接
            .hotSearchUrl(sbmyHotSearchDO.getHotSearchUrl())
            //物理主键
            .id(sbmyHotSearchDO.getId()).build();
    }
}
