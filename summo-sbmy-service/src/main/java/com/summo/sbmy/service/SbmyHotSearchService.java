package com.summo.sbmy.service;

import java.util.List;

import com.summo.sbmy.common.model.dto.HotSearchDTO;
import com.summo.sbmy.common.page.Page;
import com.summo.sbmy.dao.entity.SbmyHotSearchDO;

public interface SbmyHotSearchService {

    /**
     * 保存热搜数据到数据库
     *
     * @param sbmyHotSearchDOList 数据库
     * @return 操作状态
     */
    Boolean saveCache2DB(List<SbmyHotSearchDO> sbmyHotSearchDOList);


    /**
     * 根据标题查询热搜
     *
     * @param title    标题
     * @param pageNum  页码
     * @param pageSize 页大小
     * @return 热搜
     */
    Page<HotSearchDTO> pageQueryHotSearchByTitle(String title, Integer pageNum, Integer pageSize);

}
