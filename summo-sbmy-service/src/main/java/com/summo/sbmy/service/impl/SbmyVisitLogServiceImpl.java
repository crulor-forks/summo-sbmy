package com.summo.sbmy.service.impl;

import java.util.Date;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.summo.sbmy.common.model.dto.VisitorCountDTO;
import com.summo.sbmy.dao.AbstractBaseDO;
import com.summo.sbmy.dao.entity.SbmyVisitLogDO;
import com.summo.sbmy.dao.repository.SbmyVisitLogRepository;
import com.summo.sbmy.service.SbmyVisitLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SbmyVisitLogServiceImpl implements SbmyVisitLogService {
    @Autowired
    private SbmyVisitLogRepository sbmyVisitLogRepository;

    @Override
    public VisitorCountDTO queryVisitorCount(Date startTime, Date endTime) {
        int todayPv = sbmyVisitLogRepository.count(
            new QueryWrapper<SbmyVisitLogDO>().lambda().between(AbstractBaseDO::getGmtCreate, startTime, endTime));
        int todayUv = sbmyVisitLogRepository.queryUvByStartTimeAndEndTime(startTime, endTime);
        int allPv = sbmyVisitLogRepository.count();
        int allUv = sbmyVisitLogRepository.queryUv();
        return VisitorCountDTO.builder()
            .todayPv(todayPv)
            .todayUv(todayUv)
            .allPv(allPv)
            .allUv(allUv)
            .build();
    }
}
