package com.summo.sbmy.service;

import java.util.Date;

import com.summo.sbmy.common.model.dto.VisitorCountDTO;

public interface SbmyVisitLogService {

    /**
     * 查询pv/uv
     *
     * @param startTime 开始时间
     * @param entTime   结束时间
     * @return pv/uv
     */
    VisitorCountDTO queryVisitorCount(Date startTime, Date entTime);
}
