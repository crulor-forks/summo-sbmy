package com.summo.sbmy.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.summo.sbmy.common.model.dto.HotSearchDTO;
import com.summo.sbmy.common.page.Page;
import com.summo.sbmy.dao.AbstractBaseDO;
import com.summo.sbmy.dao.entity.SbmyHotSearchDO;
import com.summo.sbmy.dao.repository.SbmyHotSearchRepository;
import com.summo.sbmy.service.SbmyHotSearchService;
import com.summo.sbmy.service.convert.HotSearchConvert;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SbmyHotSearchServiceImpl implements SbmyHotSearchService {

    @Autowired
    private SbmyHotSearchRepository sbmyHotSearchRepository;

    @Override
    public Boolean saveCache2DB(List<SbmyHotSearchDO> sbmyHotSearchDOS) {
        if (CollectionUtils.isEmpty(sbmyHotSearchDOS)) {
            return Boolean.TRUE;
        }
        //查询当前数据是否已经存在
        List<String> searchIdList = sbmyHotSearchDOS.stream().map(SbmyHotSearchDO::getHotSearchId).collect(
            Collectors.toList());
        List<SbmyHotSearchDO> sbmyHotSearchDOList = sbmyHotSearchRepository.list(
            new QueryWrapper<SbmyHotSearchDO>().lambda().in(SbmyHotSearchDO::getHotSearchId, searchIdList));
        //过滤已经存在的数据
        if (CollectionUtils.isNotEmpty(sbmyHotSearchDOList)) {
            List<String> tempIdList = sbmyHotSearchDOList.stream().map(SbmyHotSearchDO::getHotSearchId).collect(
                Collectors.toList());
            sbmyHotSearchDOS = sbmyHotSearchDOS.stream().filter(
                sbmyHotSearchDO -> !tempIdList.contains(sbmyHotSearchDO.getHotSearchId())).collect(Collectors.toList());
        }
        if (CollectionUtils.isEmpty(sbmyHotSearchDOS)) {
            return Boolean.TRUE;
        }
        log.info("本次新增[{}]条数据", sbmyHotSearchDOS.size());
        //批量添加
        return sbmyHotSearchRepository.saveBatch(sbmyHotSearchDOS);
    }

    @Override
    public Page<HotSearchDTO> pageQueryHotSearchByTitle(String title, Integer pageNum, Integer pageSize) {
        //设置分页参数
        PageHelper.startPage(pageNum, pageSize);
        //查询热搜
        List<SbmyHotSearchDO> sbmyHotSearchDOS = sbmyHotSearchRepository.list(
            new QueryWrapper<SbmyHotSearchDO>().lambda().like(SbmyHotSearchDO::getHotSearchTitle, "%" + title + "%")
                .orderByDesc(AbstractBaseDO::getGmtCreate));
        if (CollectionUtils.isEmpty(sbmyHotSearchDOS)) {
            return Page.emptyPage();
        }
        //对象转换
        return Page.resetPage(sbmyHotSearchDOS, sbmyHotSearchDOS.stream().map(HotSearchConvert::toDTOWhenQuery)
            .collect(Collectors.toList()));
    }
}
