package com.summo.sbmy.common.base;

/**
 * 错误类型
 */
public interface ErrorTypes {

    String SYSTEM = "0";

    String BIZ = "1";

    String THIRD_PARTY = "2";
}