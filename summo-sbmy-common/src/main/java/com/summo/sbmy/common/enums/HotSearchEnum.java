package com.summo.sbmy.common.enums;

import java.util.EnumSet;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HotSearchEnum {

    /**
     * 抖音
     */
    DOUYIN("DOUYIN", "抖音"),
    BAIDU("BAIDU", "百度"),
    ZHIHU("ZHIHU", "知乎"),
    SOUGOU("SOUGOU", "搜狗"),
    BILIBILI("BILIBILI", "B站");

    private String code;
    private String desc;

    public static HotSearchEnum of(byte code) {
        return EnumSet.allOf(HotSearchEnum.class).stream().filter(x -> x.code.equals(code)).findFirst().orElse(null);
    }

}
