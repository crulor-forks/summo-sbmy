package com.summo.sbmy.common.cache;

import java.util.Map;

import com.google.common.collect.Maps;
import com.summo.sbmy.common.model.dto.HotSearchDetailDTO;

public class SbmyHotSearchCache {

    /**
     * 热搜缓存
     */
    public static final Map<String, HotSearchDetailDTO> CACHE_MAP = Maps.newHashMap();
}
